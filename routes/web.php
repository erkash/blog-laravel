<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', ['uses' => 'HomeController@index', 'as' => 'home']);
Route::post('store', ['uses' => 'HomeController@store', 'as' => 'store']);
Route::post('message/{id}/update', ['uses' => 'HomeController@update', 'as' => 'update'])->where(['id' => '[0-9]+']);
Route::get('message/{id}/edit', ['uses' => 'HomeController@edit', 'as' => 'message.edit'])->where(['id' => '[0-9]+']);
Route::get('message/{id}/delete', ['uses' => 'HomeController@delete', 'as' => 'delete'])->where(['id' => '[0-9]+']);