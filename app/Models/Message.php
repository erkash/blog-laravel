<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Message
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static Builder|\App\Models\Message whereCreatedAt($value)
 * @method static Builder|\App\Models\Message whereEmail($value)
 * @method static Builder|\App\Models\Message whereId($value)
 * @method static Builder|\App\Models\Message whereMessage($value)
 * @method static Builder|\App\Models\Message whereName($value)
 * @method static Builder|\App\Models\Message whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Message extends Model
{
    protected $fillable = ['name', 'email', 'message'];
}
