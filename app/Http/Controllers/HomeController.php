<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessage;
use App\Models\Message;

class HomeController extends Controller
{
    /**
     * @var Message
     */
    private $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    public function index()
    {
        return view('pages.messages.index', [
            'title' => 'Блог на Laravel 5.5',
            'page_title' => 'Блог на Ларавел',
            'messages' => Message::latest('updated_at')->paginate(3),
            'count' => Message::all()->count()
        ]);
    }

    public function edit($id)
    {
        return view('pages.messages.edit', [
            'title' => 'Блог на Laravel 5.5',
            'page_title' => 'Блог на Ларавел',
            'message' => $this->message->find($id)
        ]);
    }

    public function store(StoreMessage $request)
    {
        $this->message->create($request->all());
        return redirect()->route('home');
    }

    public function update(StoreMessage $request, $id)
    {
        $message = $this->message->find($id);
        $message->name = $request->input('name');
        $message->email = $request->input('email');
        $message->message = $request->input('message');
        $message->save();
        return redirect()->route('home');
    }

    public function delete($id)
    {
        $message = $this->message->find($id);
        $message->delete();
        return redirect()->route('home');
    }
}
