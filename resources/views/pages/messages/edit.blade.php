@extends('index')

@section('content')
    <form method="POST" id="id-form_messages" action="{{route('update', ['id' => $message->id])}}">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Имя: *</label>
            <input class="form-control" name="name" type="text" id="name" value="{{$message->name}}">
        </div>

        <div class="form-group">
            <label for="email">E-mail:</label>
            <input class="form-control" name="email" type="email" id="email" value="{{$message->email}}">
        </div>

        <div class="form-group">
            <label for="message">Сообщение: *</label>
            <textarea class="form-control" rows="5" name="message" cols="50" id="message">{{$message->message}}</textarea>
        </div>

        <div class="form-group">
            <input class="btn btn-primary" type="submit" value="Изменить">
        </div>
    </form>
@stop