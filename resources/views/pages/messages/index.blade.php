@extends('index')

@section('content')
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @include('_common._form')

    <div class="text-right"><b>Всего сообщений:</b> <i class="badge">{{ $count }}</i></div>
    <br/>

    <div class="messages">
        @foreach($messages as $message)
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <span>{{$message->name}}</span>
                    <span class="pull-right label label-info">{{$message->created_at->format('H:i:s / d.m.Y')}}</span>
                </h3>
            </div>

            <div class="panel-body">
                {{$message->message}}
                    <div class="pull-right">
                        <a href="{{route('message.edit', ['id' => $message->id])}}" class="btn btn-info">
                            <i class="glyphicon glyphicon-pencil"></i>
                        </a>
                        <a href="{{route('delete', ['id' => $message->id])}}" class="btn btn-danger">
                            <i class="glyphicon glyphicon-trash"></i>
                        </a>
                    </div>
            </div>
        </div>
        @endforeach
    </div>
    {{ $messages->links() }}
@stop
